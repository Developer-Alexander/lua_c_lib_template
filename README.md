This is a template for lua libraries written in C.

# Build

1. Set the name (`PROJECT_NAME`) and version (`PROJECT_MAJOR_VERSION, PROJECT_MINOR_VERSION, PROJECT_PATCH_VERSION`) of your project in `CMakeLists.txt` (e.g. MyAwesomeLib 1.0.0)

- `PROJECT_NAME` must be a valid part of a C function

2. Set the version of lua (`LUA_MAJOR_VERSION, LUA_MINOR_VERSION, LUA_PATCH_VERSION`) in `CMakeLists.txt` (e.g. 5.2.3)

3. Optional: Set a custom output directory (`OUTPUT_DIRECTORY`) in `CMakeLists.txt`

4. Put your source files into `/src` subdirectory.

5. Configure the project via cmake

## Requirements

### Windows

1. git
2. CMake >= 3.21
3. Visual Studio (>= 2019 is recommended)

### Linux

1. git
2. CMake >= 3.21
3. g++
